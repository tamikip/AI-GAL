# 在开始使用之前
- 首先确保您有一台4GB显存以上，10系以上的显卡。用于运行ai绘画与ai语音
- chatgpt的密钥（或者本地部署LLM）
- 安装好以下程序：
1. stable diffusion
2. gpt-sovits(可以用其他替代，但是要自己改代码)
3. rembg（github上面有）
4. 我的资源包（右侧）
5. 配置,填写config.ini
6. 在game目录下新建一个images文件夹


## 正式开始
### 在运行程序之前，你首先要：

1. 运行stable diffusion的api
2. 运行gpt-sovits的api
3. 运行rembg的api

### 接下来就可以直接打开程序了，资源加载速度取决于您的电脑配置，请耐心等待


